# KeyboardSdk
Library for Multi-Lingual Keyboards in Android.
 
## Setup

Add this to your project build.gradle
``` gradle
allprojects {
    repositories {
        maven { url 'https://jitpack.io' }
    }
}
``` 

#### Version
[![](https://jitpack.io/v/org.bitbucket.droidres/keyboardsdk.svg)](https://jitpack.io/#org.bitbucket.droidres/keyboardsdk)
```gradle
dependencies {
   	implementation 'org.bitbucket.droidres:keyboardsdk:x.y' 
}
```   

In your layout.xml file:
#### Usage method
```xml 
    <?xml version="1.0" encoding="utf-8"?>
	<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
		xmlns:tools="http://schemas.android.com/tools"
		android:id="@+id/activity_main"
		android:layout_width="match_parent"
		android:layout_height="match_parent" >


		<com.keyboardsdk.KeyboardSdkView
			android:id="@+id/keyboardView"
			android:layout_width="match_parent"
			android:layout_height="wrap_content"
			android:layout_alignParentBottom="true"
            android:keyBackground="@color/keyboard_key_background"
            android:keyTextColor="@color/keyboard_key_text"
            android:background="@color/keyboard_background"
			android:visibility="gone"/>
	</RelativeLayout>
```

In your Activity.class:
#### Usage method
```java 
    EditText messageEditText = findViewById(R.id.messageEditText);
	KeyboardSdkView mKeyboardView = findViewById(R.id.keyboardView);

	keyboardSdk = KeyboardSdk.getInstance(this)
           .setResources(mKeyboardView, messageEditText)
           .build();

	keyboardSdk.selectKeyboard(KeyboardUtil.DEFAULT);
	
	@Override
    protected void onResume() {
        super.onResume();
        keyboardSdk.onResume();
    }
```
 
#### Keyboard Languages Support
- Default (Default Keyboard)
- Hindi
- Gujarati
- Marathi
- Kannada
- Tamil
- Punjabi

