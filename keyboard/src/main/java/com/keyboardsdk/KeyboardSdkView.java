package com.keyboardsdk;

import android.content.Context;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

/**
 * @author Abhijit Rao
 */
public class KeyboardSdkView extends KeyboardView
{

	public KeyboardSdkView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void showWithAnimation(Animation animation) {
		animation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				/* no-op */
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				/* no-op */
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				setVisibility(View.VISIBLE);
			}
		});
		
		setAnimation(animation);
	}

}
