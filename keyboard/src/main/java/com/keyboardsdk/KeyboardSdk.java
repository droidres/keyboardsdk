package com.keyboardsdk;

import android.app.Activity;
import android.content.Context;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Build;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * @author Abhijit Rao
 */
public class KeyboardSdk {
    private final Activity activity;
    private int mLanguage;
    private KeyboardSdkView mKeyboardView;
    private EditText messageEditText;
    private Keyboard mKeyboard;
    private long mVirtualKeyboardHideDelayTime;

    private KeyboardSdk(Activity activity) {
        this.activity = activity;
        this.mLanguage = KeyboardUtil.DEFAULT;
        this.mVirtualKeyboardHideDelayTime = 0;
    }

    public static KeyboardSdk getInstance(Activity activity) {
        return new KeyboardSdk(activity);
    }

    public void hideSoftKeyboard(final Activity activity) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager inputMethodManager =
                        (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(messageEditText.getWindowToken(), 0);
            }
        }, mVirtualKeyboardHideDelayTime);
        if (mKeyboardView != null) {
            mKeyboardView.setVisibility(View.GONE);
        }
    }

    public KeyboardSdk setResources(KeyboardSdkView mKeyboardView, EditText messageEditText) {
        this.mKeyboardView = mKeyboardView;
        this.messageEditText = messageEditText;
        return this;
    }

    public KeyboardSdk setLanguage(int mLanguage) {
        this.mLanguage = mLanguage;
        if (mLanguage != KeyboardUtil.DEFAULT) {
            setTouchListener();
        } else {
            messageEditText.setOnTouchListener(null);
        }
        return this;
    }

    public KeyboardSdk setVirtualKeyboardHideDelayTime(long mVirtualKeyboardHideDelayTime) {
        this.mVirtualKeyboardHideDelayTime = mVirtualKeyboardHideDelayTime;
        return this;
    }

    public KeyboardSdk build() {
//        messageEditText.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                v.getParent().requestDisallowInterceptTouchEvent(true);
//                switch (event.getAction() & MotionEvent.ACTION_MASK) {
//                    case MotionEvent.ACTION_UP:
//                        v.getParent().requestDisallowInterceptTouchEvent(false);
//                        break;
//                }
//                return false;
//            }
//        });
        return this;
    }

    public void onResume() {
        mKeyboardView.setVisibility(View.GONE);
    }

    public void openKeyboard() {
        showKeyboard();
    }

    private void showKeyboard() {
        // Do not show the preview balloons
        mKeyboardView.setPreviewEnabled(false);

        if (mLanguage != KeyboardUtil.DEFAULT) {
            hideSoftKeyboard(activity);

            if (mLanguage == KeyboardUtil.HINDI) {
                mKeyboard = new Keyboard(activity, R.xml
                        .kbd_hin1);
                showKeyboardWithAnimation();
                mKeyboardView.setVisibility(View.VISIBLE);
                mKeyboardView.setKeyboard(mKeyboard);

            } else if (mLanguage == KeyboardUtil.GUJARATI) {
                if (KeyboardUtil.isLangSupported(activity, "ગુજરાતી")) {
                    mKeyboard = new Keyboard(activity, R.xml.kbd_guj1);
                    showKeyboardWithAnimation();
                    mKeyboardView.setVisibility(View.VISIBLE);
                    mKeyboardView.setKeyboard(mKeyboard);
                } else {
                    KeyboardUtil.displayAlert(activity, "Keyboard", "Gujarati keyboard is not supported "
                            + "by your device");
                    //Reset mLanguage selection
                    showKeyboard();
                    mKeyboard = new Keyboard(activity, R.xml
                            .kbd_hin1);
                    mKeyboardView.setVisibility(View.GONE);

                    //Show Default Keyboard
                    InputMethodManager imm =
                            (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(messageEditText, 0);
                    messageEditText.setOnTouchListener(null);
                }
            } else if (mLanguage == KeyboardUtil.MARATHI) {
                mKeyboard =
                        new Keyboard(activity, R.xml.kbd_mar1);
                showKeyboardWithAnimation();
                mKeyboardView.setVisibility(View.VISIBLE);
                mKeyboardView.setKeyboard(mKeyboard);
            } else if (mLanguage == KeyboardUtil.KANNADA) {
                mKeyboard =
                        new Keyboard(activity, R.xml.kbd_knd1);
                showKeyboardWithAnimation();
                mKeyboardView.setVisibility(View.VISIBLE);
                mKeyboardView.setKeyboard(mKeyboard);
            } else if (mLanguage == KeyboardUtil.TAMIL) {
                mKeyboard = new Keyboard(activity, R.xml
                        .kbd_tam1);
                showKeyboardWithAnimation();
                mKeyboardView.setVisibility(View.VISIBLE);
                mKeyboardView.setKeyboard(mKeyboard);
            } else if (mLanguage == KeyboardUtil.PUNJABI) {
                if (KeyboardUtil.isLangSupported(activity, "ਪੰਜਾਬੀ ਦੇ")) {

                    mKeyboard = new Keyboard(activity, R.xml
                            .kbd_punj1);
                    showKeyboardWithAnimation();
                    mKeyboardView.setVisibility(View.VISIBLE);
                    mKeyboardView.setKeyboard(mKeyboard);
                } else {
                    KeyboardUtil.displayAlert(activity, "Keyboard", "Punjabi keyboard is not supported "
                            + "by your device");

                    //Reset mLanguage selection
                    showKeyboard();
                    mKeyboard = new Keyboard(activity, R.xml
                            .kbd_hin1);
                    mKeyboardView.setVisibility(View.GONE);

                    //Show Default Keyboard
                    InputMethodManager imm =
                            (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(messageEditText, 0);
                    messageEditText.setOnTouchListener(null);
                }
            }

            mKeyboardView.setOnKeyboardActionListener(new BasicOnKeyboardActionListener(
                    activity,
                    messageEditText,
                    mKeyboardView));

            showCursorInEdittext(false);

            setTouchListener();
        } else {
            mKeyboardView.setVisibility(View.GONE);

            showDefaultKeyboard();
        }
    }
    private void showCursorInEdittext(boolean isKeyboardVisible) {
        messageEditText.requestFocus();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            messageEditText.setShowSoftInputOnFocus(isKeyboardVisible);
        } else {
            if (!isKeyboardVisible)
                hideSoftKeyboard(activity);
        }
    }

    private void setTouchListener() {
        messageEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                showKeyboard();

//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_UP:
//                        Layout layout = ((EditText) v).getLayout();
//                        float x = event.getX() + messageEditText.getScrollX();
//                        float y = event.getY() + messageEditText.getScrollY();
//                        int line = layout.getLineForVertical((int) y);
//
//                        int offset = layout.getOffsetForHorizontal(line, x);
//                        if (offset > 0)
//                            if (x > layout.getLineMax(0))
//                                messageEditText
//                                        .setSelection(offset);     // touch was at end of text
//                            else
//                                messageEditText.setSelection(offset - 1);
//
//                        messageEditText.setCursorVisible(true);
//                        break;
//                }
                return true;
            }
        });
    }

    private void showDefaultKeyboard() {
        //Show Default Keyboard
//        messageEditText.requestFocus();
//        messageEditText.setShowSoftInputOnFocus(true);
        showCursorInEdittext(true);

        InputMethodManager imm =
                (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(messageEditText, 0);
        if (messageEditText != null)
            messageEditText.setOnTouchListener(null);
    }

    private void showKeyboardWithAnimation() {
        if (mKeyboardView.getVisibility() == View.GONE) {
            Animation animation = AnimationUtils
                    .loadAnimation(activity,
                            R.anim.slide_from_bottom);
            mKeyboardView.showWithAnimation(animation);
        }
    }


    private class BasicOnKeyboardActionListener implements KeyboardView.OnKeyboardActionListener {

        EditText editText;
        KeyboardSdkView displayKeyboardView;
        private Activity mTargetActivity;

        public BasicOnKeyboardActionListener(Activity targetActivity, EditText editText, KeyboardSdkView displayKeyboardView) {
            mTargetActivity = targetActivity;
            this.editText = editText;
            this.displayKeyboardView = displayKeyboardView;
        }

        @Override
        public void swipeUp() {
            /* no-op */
        }

        @Override
        public void swipeRight() {
            /* no-op */
        }

        @Override
        public void swipeLeft() {
            /* no-op */
        }

        @Override
        public void swipeDown() {
            /* no-op */
        }

        @Override
        public void onText(CharSequence text) {
            int cursorPosition = editText.getSelectionEnd();
            String previousText = editText.getText().toString();
            String before, after;
            if (cursorPosition < previousText.length()) {
                before = previousText.substring(0, cursorPosition);
                after = previousText.substring(cursorPosition);
            } else {
                before = previousText;
                after = "";
            }
            editText.setText(before + text + after);
            editText.setSelection(cursorPosition + 1);
        }

        @Override
        public void onRelease(int primaryCode) {
            /* no-op */
        }

        @Override
        public void onPress(int primaryCode) {
            /* no-op */
        }

        @Override
        public void onKey(int primaryCode, int[] keyCodes) {
            switch (primaryCode) {
                case 66:
                case 67:
                    long eventTime = System.currentTimeMillis();
                    KeyEvent event =
                            new KeyEvent(eventTime, eventTime, KeyEvent.ACTION_DOWN, primaryCode, 0, 0, 0, 0,
                                    KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE);

                    mTargetActivity.dispatchKeyEvent(event);
                    break;
                case -106:
                    displayKeyboardView
                            .setKeyboard(new Keyboard(mTargetActivity, R.xml.kbd_hin2));
                    break;
                case -107:
                    displayKeyboardView
                            .setKeyboard(new Keyboard(mTargetActivity, R.xml.kbd_hin1));
                    break;
                case -108:
                    displayKeyboardView
                            .setKeyboard(new Keyboard(mTargetActivity, R.xml.kbd_guj2));
                    break;
                case -109:
                    displayKeyboardView.setKeyboard(new Keyboard(mTargetActivity, R.xml.kbd_guj1));
                    break;
                case -110:
                    displayKeyboardView.setKeyboard(new Keyboard(mTargetActivity, R.xml.kbd_mar2));
                    break;
                case -111:
                    displayKeyboardView.setKeyboard(new Keyboard(mTargetActivity, R.xml.kbd_mar1));
                    break;
                case -112:
                    displayKeyboardView.setKeyboard(new Keyboard(mTargetActivity, R.xml.kbd_knd2));
                    break;
                case -113:
                    displayKeyboardView.setKeyboard(new Keyboard(mTargetActivity, R.xml.kbd_knd1));
                    break;
                case -114:
                    displayKeyboardView.setKeyboard(new Keyboard(mTargetActivity, R.xml.kbd_tam2));
                    break;
                case -115:
                    displayKeyboardView.setKeyboard(new Keyboard(mTargetActivity, R.xml.kbd_tam1));
                    break;
                case -116:
                    displayKeyboardView.setKeyboard(new Keyboard(mTargetActivity, R.xml.kbd_punj2));
                    break;
                case -117:
                    displayKeyboardView.setKeyboard(new Keyboard(mTargetActivity, R.xml.kbd_punj1));
                    break;
                default:
                    break;
            }
        }
    }
}
